import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SampleFormComponent } from './sample-form.component';
import { RouterModule, Routes } from '@angular/router';
import { SharedModule } from 'src/app/shared/shared.module';
import { ReactiveFormsModule } from '@angular/forms';

const routes: Routes = [
  { path: '', component:SampleFormComponent},
];

@NgModule({
  declarations: [SampleFormComponent],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    SharedModule,
    ReactiveFormsModule
  ]
})
export class SampleFormModule { }
