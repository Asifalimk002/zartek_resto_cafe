import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-sample-form',
  templateUrl: './sample-form.component.html',
  styleUrls: ['./sample-form.component.scss']
})
export class SampleFormComponent implements OnInit {

  tabIndex: number = 0;

  sampleForm: FormGroup;

  constructor(public formBuilder: FormBuilder) { }



  ngOnInit(): void {

    this.sampleForm = this.formBuilder.group({
      'name': ['', Validators.compose([Validators.required])],
      'username': ['', Validators.compose([Validators.required])],
      'billing': [''],
      'pincode': [''],
      'phonenumber': ['', Validators.compose([Validators.required])],
      'cardnumber': ['', Validators.compose([Validators.required])],
      'expiry': ['', Validators.compose([Validators.required])],
      'cvv': ['', Validators.compose([Validators.required])],
      'nameoncard': ['', Validators.compose([Validators.required])]
    });
  }

  /**
   * 
   * called when submit form
   */
  submit(): void {
    console.log(this.sampleForm.value)
  }

}
