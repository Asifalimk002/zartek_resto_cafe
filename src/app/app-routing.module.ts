import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PagesComponent } from './pages/pages.component';

export const routes: Routes = [
  {
    path: '', component: PagesComponent, children: [
      { path: '', redirectTo: 'sample-form', pathMatch: 'full' },
      { path: 'sample-form', loadChildren: () => import('./pages/sample-form/sample-form.module').then(m => m.SampleFormModule) },
    ]
  }
]

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
